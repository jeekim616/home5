import java.util.*;
// The initial homework skeleton derives from:
//   - https://bitbucket.org/itc_algorithms/home5/
// A previous attempt at
//   - https://bitbucket.org/furunkel027/kodu5/
// Tagged I231 to enable future searches.

/**
 * Task: build 1) a tree from the right parenthetic string representation
 * (return the root node of the tree) and
 * a method to construct the left parenthetic string representation
 *    of a tree represented by the root node this.
 * @author The empty skeleton and unit tests are attributed to jpoial
 * @author A particular solution prepared by jeekim616
 */
public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   // Not all humanoids think mathematically
   // The delimiter constants:
   static final char sulgLahti = '(';
   static final char sulgKinni = ')';
   static final char koma = ',';

   // Constructor
   Node(String n, Node d, Node r) {
      name = n;
      firstChild = d; // d as dive? Unlucky semantics :(
      nextSibling = r; // absolutely unlucky semantics.
   }

    /**
     * checks the string Under Test (sUT) for error conditions
     *   and creates exceptions
     * @param stringUT The String to be checked
     * @return true if the string was flawless
     */
    public static boolean isKosher(String stringUT) {
        // The style: Both print and exceptions
        // so that the reason is clearly visible during the tests

        // ERROR 01 works against testTwoCommas
        if (stringUT.contains(",,")) {
            System.out
                    .println("ERROR: two consequtive commas found in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has consecutive commas!");
        }

        // ERROR 02 is not catched by ANY of the unit tests
         if (stringUT.contains(")(")) {
            System.out
                    .println("ERROR: a reversed pair of parentheses met in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has reversed pair of parentheses!");
        }

        // ERROR 03 works agains testComma1 and something more
       if  (stringUT.contains("(,")){
            System.out
                    .println("ERROR: a faulty sequence of comma+parenthesis (type A) met in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has a faulty sequence of comma+parentheses!");
        }

        // ERROR 04 is not catched by ANY of the unit tests
        if  (stringUT.contains(",)")) {
            System.out
                    .println("ERROR: a faulty sequence of comma+parenthesis (type B) met in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has a faulty sequence of comma+parentheses!");
        }

        // ERROR 05 is not catched by ANY of the unit tests
         if  (stringUT.contains("),")) {
            System.out
                    .println("ERROR: a faulty sequence of parenthesis+comma (type C) met in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has a faulty sequence of parentheses+comma!");
        }

        // ERROR 06 works against testTab1 . But see the next.
        if (stringUT.contains("\t")) {
            System.out
                    .println("ERROR: at least one TAB symbol found in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| includes a prohibited TAB symbol!");
        }

        // ERROR 07 works against testSpaceInNodeName but is a little bit more generic
         if (stringUT.contains(" ")) {
            System.out
                    .println("ERROR: at least one SPACE symbol found in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| includes a prohibited SPACE symbol!");
        }

        // ERROR 08 works agains testEmptySubtree
        if (stringUT.contains("()")) {
            System.out
                    .println("ERROR: an empty pair of round brackets found in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has empty pair of round brackets!");
        }

        // ERROR 09 - unit test are not able to catch this specific condition
        if ( stringUT.charAt(0) == sulgKinni ) { // testWeirdBrackets
            System.out
                    .println("ERROR: input string starts with a closing bracket!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| starts with a closing bracket!");
        }

        // ERROR 10 - no unit test is able to catch this condition
         if ( stringUT.charAt(stringUT.length()-1) == sulgLahti ) {
            System.out
                    .println("ERROR: input string starts with a closing bracket!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| starts with a closing bracket!");
        }

        // ERROR 11 - no unit test is able to catch this condition (testSpaceInNodeName + trim)
         String trimmedS = stringUT.replaceAll("\\s", "");
        if (trimmedS.contains("(,)")) {
            System.out
                    .println("ERROR: an orphan comma surrounded by round brackets found in input string!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| has an orphan comma surrounded by round brackets!");
        }

        // ERROR 12 works against testInputWithDoubleBrackets and extra triple and quadruple and etc
         if (stringUT.matches(".*[(]{2}[\\w]+,[\\w]+[)]{2}.*")) { // A wild regexp
            System.out
                    .println("ERROR: matrjoshka parentheses around an argument are not allowed!");
            throw new IllegalArgumentException("EXCEPTION: string |" + stringUT
                    + "| contains matrjoshka parenthesis around an argument!");
        }
        // Are unpaired parentheses directly trapped? ;)
        System.out.println("=> OK");
        return true;
    }

    // =====================

    /**
     * Helper method to distinguish Node names from operator commands
     * @param s the String under test
     * @return true if it was a command. (See constants)
     */
    public static boolean isCommand (String s) {

        if ( (s.length() == 1)
                && ( (s.charAt(0) == koma)
                || (s.charAt(0) == sulgLahti)
                || (s.charAt(0) == sulgKinni)  )  ) {
            return true; // should be a command
        }
        else { // otherwise a nodename
            return false;
        }
    }

    /**
     * Will split the string / tokenise it together with delimiters
     * @param p to be tokenized
     * @return pointer to the resulting ArrayList
     */
    // ========================================================
    public static ArrayList tokenizeTheInput(String p) {
        String delims = "(),";
        String splitString = p;

        ArrayList<String> result = new ArrayList<String>(1);
        StringTokenizer row = new StringTokenizer(splitString, delims, true); // true = delimiters included
        while (row.hasMoreTokens()) {
            String elem = row.nextToken();
            result.add(elem);
        }
        return result;
    }

    /**
     * A helper method to delete the first element of the token row and return it
     * @param l list to work with
     * @return the element just deleted from the list
     */
    public static String oneOff(ArrayList<String> l) {
        int size = l.size();
        String firstToken = l.get(0);
        l.remove(0);
        return firstToken;
    }

    /**
     * A Helper method to make program code more readable
     * @return an "empty" Node
     */
    public static Node nullNode() {
        return new Node(null, null, null);
    }

    /**
     * Method to transfer a PostFix String to a Tree
     * The RPN program logic was imitated from here: https://git.wut.ee/i231/home5.git
     * These works were also consulted (albeit with PreFix form):
     *   - https://github.com/herrbpl/i231-homework5
     *   - https://bitbucket.org/RUusjarv/homework5
     *   - http://enos.itcollege.ee/~ylari/I231/
     * @param s String to be converted
     * @return pointer to the root Node of the tree
     */
   public static Node parsePostfix (String s) {
       Node currentNode = nullNode();
       ArrayList<String> tokenList = new ArrayList<String>(10);
       ArrayList<String> tmp = new ArrayList<String>(10);
       Stack<Node> lifo = new Stack();
       boolean achtung = false; // to signal root node overwriting
       int i=0;
       int j=0;
       tokenList = tokenizeTheInput(s);
       tmp = tokenizeTheInput(s); // a parallel construction to debug more nicely

       System.out.println();
       System.out.print("Input: |" + s + "|  ");
       // System.out.println();

       isKosher(s); // Error handling

       // This will work against testSingleRoot()
        if ( tokenList.size() == 1) {
        return new Node (tokenList.get(0), null, null);
        }

       // DEBUG System.out.println("Starting from  Level=" + j +".");
        for (String token: tokenList) {

            // DEBUG System.out.print("token =" + token + " from ");
            oneOff(tmp);
            // debug(tmp);

            if (isCommand(token)) {

                    // case syntax used to get a clearer (albeit much longer) code
                switch(token.charAt(0)) {
                    case (sulgLahti) : {
                        // DEBUG  i++; // tokenlist iterator
                        j--; // Level indicator
                        // DEBUG  System.out.println("step=" + i + ". Dive to Level=" + j +". Go Deeper!");
                        // DEBUG  System.out.println();
                        System.out.println("DEBUG nodename to dive any further: " +currentNode.firstChild);
                         // secret ERROR 15
                        if (currentNode.name == null) {
                            lifo.push(currentNode);
                            currentNode.firstChild = nullNode();
                            currentNode = currentNode.firstChild;
                            break;
                        } else {
                            throw new RuntimeException("Exception: trying to dive left branch again. String: " + s);
                        }
                    }

                    case (sulgKinni) : {
                        // DEBUG i++;
                        j++;
                        // DEBUG System.out.println("step=" + i + ". Return Upwards!");
                        // DEBUG System.out.println();
                            currentNode = lifo.pop();
                        break;
                    }

                    case (koma) : {
                        // DEBUG i++;
                        // DEBUG System.out.println("step=" + i + ". Look on the right and collect siblings!");
                        // DEBUG System.out.println();
                        currentNode.nextSibling = nullNode();
                        currentNode = currentNode.nextSibling;
                        break;
                    }

                    default : throw new RuntimeException("EXCEPTION: The system made Boo-Boo at case default branch.");
                }

            } else {
                if (achtung) { // ERROR 13 satisfying BOTH testComma2 testComma3
                    // WARNING - testComma2 testComma3 are not actually about commas but token interleave faults
                    throw new RuntimeException("Hello mama, already been here. Not overwriting the Root Node");
                }
                // DEBUG i++;
                // DEBUG System.out.println("step=" + i + ". This node is at level=" + j + " is named " + token + ".");
                // DEBUG System.out.println();
                if (j==0) achtung = true; // Zero level is of interest just once
                currentNode.name = token;

            }

        }

      return currentNode;
   }


    // Õpik (http://enos.itcollege.ee/~jpoial/algoritmid/puud.html) ütleb:
    // Eesjärjestus (pre-order):
    // Töödelda juur
    // Töödelda juure alampuud järjestuses vasakult paremale //

    /**
     * The method translates the Node representation into a pre-order String
     * Inspirations:
     * - http://stackoverflow.com/questions/10766492/reverse-the-arraylist-in-simplest-way
     * - via https://bitbucket.org/furunkel027/kodu5/
     * - method rightParentheticRepresentation() below (order changed, root put first)
     * @return String representing the tree
     */
    public String leftParentheticRepresentation() {
        StringBuilder tagastus = new StringBuilder();
        boolean kasLapsiOn = false;

        tagastus.append(name).toString(); // Juurtipp kohe väljundisse
        Node element = firstChild; // Ehk siis mitte asi ise vaid ta esmasündinu
        if (firstChild != null) { // Avanev sulg tähistab, et on kuhu laskuda
            kasLapsiOn = true; // Abimuutuja, et mitte teist korda kontrollida
            tagastus.append(sulgLahti); // rekursiooni eel-tegevus
        }
        while (element != null) { // Rekursiivselt muudkui allapoole
            tagastus.append(element.leftParentheticRepresentation());
            element = element.nextSibling; // järgmine naaber loosi!
            if (element != null)
                tagastus.append(koma); // Koma: samal tasemel on naabreid veel
        }
        if (kasLapsiOn) // Heh ... tüüpiline rekursiooni järel-tegevus
            tagastus.append(sulgKinni); // All ära käidud.

        return tagastus.toString();
    }

    /**
     * An helper method to print out the content of a list
     * @param dlist the list to print out
     */
    public static void debug(ArrayList<String> dlist) {
        int length = dlist.size();
        System.out.print("Tokens: ");
        for (int i = 0; i <= (length - 1); i++) {
            String me = dlist.get(i);
            System.out.print(" |" + me + "|");
        }
        System.out.println();
    }

    // ======== an optional rightParentheticRepresentation() =========
    /**
     * This method was not required
     * The method translates the Node representation into a postfix String
     * However, it was the intermediate step to understand the logic
     * - leftParentheticRepresentation was written based on this
     * inspiration: - http://enos.itcollege.ee/~ylari/I231/Node.java
     * via - https://bitbucket.org/furunkel027/kodu5/
     * @return String representing the tree
     */
    public String rightParentheticRepresentation() {
        StringBuilder tagastus = new StringBuilder();
        boolean kasLapsiOn = false;

        Node element = firstChild; // Ehk siis mitte asi ise vaid ta esmasündinu
        if (firstChild != null) { // Avanev sulg tähistab, et on kuhu laskuda
            kasLapsiOn = true; // Abimuutuja, et mitte teist korda kontrollida
            tagastus.append(sulgLahti); // rekursiooni eel-tegevus
        }
        while (element != null) { // Rekursiivselt muudkui allapoole
            tagastus.append(element.rightParentheticRepresentation());
            element = element.nextSibling; // järgmine naaber loosi!
            if (element != null)
                tagastus.append(koma); // Koma: samal tasemel on naabreid veel
        }
        if (kasLapsiOn) // Heh ... tüüpiline rekursiooni järel-tegevus
            tagastus.append(sulgKinni); // All ära käidud.

        return tagastus.append(name).toString();
    }

    // ================= an optional TEST tree ======================
    /**
     * This method was not required. An equivalent to String whatEver = "A(B,C(D,E),F(G))";
     * It was created to check the syntax and create a pre-order tree
     * to feed the rightParentheticRepresentation()
     * @return pointer to the root node of the tree
     */
    public static Node createTestTree() {
        Node test = new Node("A", new Node("B", null, new Node("C", new Node(
                "D", null, new Node("E", null, null)), new Node("F", new Node(
                "G", null, null), null))), null);
        return test;
    }

    // ==========================================================================

    public static void main (String[] param) {
      // Bunch of test examples to use
      String s0 = "A";
      String s1 = "(B)A";
      String s = "(B1,C)A";
      String sVasak = "";
      String ss = "((A)B(C)D)E";
      // String ss = "((A)B(C)D)E";
      String ssVasak = "A(B,C(D,E),F(G))";
      String sss = "(((G,H)D,E,(I)F)B,(J)C)A";
      String sssVasak = "A(B(D(G,H),E,F(I)),C(J))";
      String x = "(,)";

      Node t = Node.parsePostfix (ss);
      String v = t.leftParentheticRepresentation();
      System.out.println("A demo: Post-order to pre-order transcription:");
      System.out.println (ss + " ==> " + v); // (B1,C)A ==> A(B1,C)

      // Node t = createTestTree();
      // String v = t.leftParentheticRepresentation();
      // System.out.println ("TestTree in  LeftParForm: ==> " + v);
      //
      // String p = t.rightParentheticRepresentation();
      // System.out.println ("TestTree in RightParForm: ==> " + p);

      // Node tt = Node.parsePostfix(s);
      // String test = tt.rightParentheticRepresentation();
      // System.out.println("Puu tagasi stringiks teisendatult: ==> " + test);

      // The original example
      // String vv = tt.leftParentheticRepresentation();
      // System.out.println (ss + " ==> " + vv); // (B1,C)A ==> A(B1,C)

   }
}
